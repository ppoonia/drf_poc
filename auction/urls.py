
from django.contrib import admin
from django.urls import path, include
from rest_framework.routers import DefaultRouter

from . import views


router = DefaultRouter()
router.register(r'auction', views.AuctionViewSet, basename='auction')
router.register(r'Bid', views.BidingViewSet, basename='bid')
router.register(r'ViewBids', views.UserBidView, basename='bid_view')
urlpatterns = router.urls

# urlpatterns
#     #path('view-auction/', views.AuctionViewSet, name="Auction-view"),
#     path('view-auction/',views.AuctionViewSet.as_view())
# ]

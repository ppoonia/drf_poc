from django.shortcuts import render
from django.http import Http404, request
from django_filters.rest_framework import DjangoFilterBackend
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework import status, viewsets, generics
from .models import Auction, Bid
from .serializers import AuctionSerializer, BiddingSerializer


class AuctionViewSet(viewsets.ModelViewSet):
    queryset = Auction.objects.all()
    serializer_class = AuctionSerializer
    # def create(self, request, serializer_class= serializer_class, **kwargs):
    #     if serializer_class.is_valid():
    #         serializer_class.save(Auction_creator=request.user)


class BidingViewSet(viewsets.ModelViewSet):
    queryset = Bid.objects.all()
    # queryset = Bid.objects.filter(Bidder = Auction.Auction_creator)
    serializer_class = BiddingSerializer


class UserBidView(viewsets.ModelViewSet):
    queryset = Bid.objects.all()
    serializer_class = BiddingSerializer

    filter_backends = (DjangoFilterBackend,)
    filter_fields = ('Bidder', 'auction_title')

    # filterset_fields = {
    #
    # }
    # def get_queryset(self):
    #     user = self.request.user
    #     return Bid.objects.filter(Bidder=user)




from .models import Auction, Bid
from rest_framework import serializers


class AuctionSerializer(serializers.ModelSerializer):
    Auction_creator = serializers.HiddenField(default=serializers.CurrentUserDefault())

    class Meta:
        model = Auction
        fields = [
            "Auction_created",
            "Auction_title",
            "Auction_description",
            "start_price",
            "Auction_creator"
        ]


class BiddingSerializer(serializers.ModelSerializer):
    Bidder = serializers.PrimaryKeyRelatedField(read_only=True, default=serializers.CurrentUserDefault())

    # auction_name = serializers.CharField(source='auction_title')
    # auction_name = AuctionSerializer(source="auction_title")

    # auction_title = serializers.StringRelatedField(many=True)
    # auction = AuctionSerializer(many=True)

    class Meta:
        model = Bid
        fields = [
            "Bidder",
            "Bid_Price",
            "auction_title",
            # "auction"
            # "auction_name"
        ]

    def save(self, **kwargs):
        """Include default for read_only field"""
        kwargs["Bidder"] = self.fields["Bidder"].get_default()
        # kwargs["auction_title"] = Auction.Auction_title

        return super().save(**kwargs)

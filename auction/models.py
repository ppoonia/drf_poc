from django.db import models
from django.contrib.auth.models import User


class Auction(models.Model):
    Auction_creator = models.ForeignKey(User, on_delete=models.CASCADE, null=True, blank=False)
    Auction_created = models.DateTimeField(auto_now_add=True)
    Auction_title = models.CharField(max_length=100, blank=True, default='')
    Auction_description = models.CharField(max_length=100, blank=True, default='')
    start_price = models.PositiveIntegerField(blank=True, null=True)

    def __str__(self):
        return str(self.Auction_title)

    # def __str__(self):
    #     return str(self.Auction_creator)


class Bid(models.Model):
    Bidder = models.CharField(max_length=100, blank=True, default='')
    Bid_Price = models.PositiveIntegerField(blank=True, null=True)
    auction_title = models.ForeignKey(Auction, on_delete=models.CASCADE, null=True, blank=False)

    def __str__(self):
        return str(self.auction_title)

    def __str__(self):
        return str(self.Bidder)

